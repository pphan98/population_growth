module PopulationGrowth
  class Client
    POPULATION_GROWTH_API = 'https://population-growth-api.herokuapp.com'

    attr_reader :access_token

    # access_token currently not used
    def initialize(access_token = nil)
      @access_token = access_token
    end

    def locations
      LocationsConnection.new(connection: connection)
    end

    private

    def connection
      @connection ||= Faraday.new(connection_options)
    end

    def connection_options
      {
        url: POPULATION_GROWTH_API,
        headers: {
          content_type: 'application/json',
          authorization: "Bearer #{access_token}"
        }
      }
    end
  end
end
