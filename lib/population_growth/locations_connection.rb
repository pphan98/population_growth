module PopulationGrowth
  class LocationsConnection
    attr_reader :connection

    def initialize(connection:)
      @connection = connection
    end

    def find(zip:)
      resp = connection.get("api/v1/locations/#{zip}")
      raise_api_error(resp) unless resp.success?
      location_data = MultiJson.load(resp.body)['location']
      Location.new(location_data)
    end

    private

    def raise_api_error(resp)
      status = resp.status
      reason = resp.reason_phrase
      fail PopulationGrowth::ApiError, "#{status}: #{reason}"
    end
  end
end
