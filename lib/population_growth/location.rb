module PopulationGrowth
  class Location
    attr_reader :data

    def initialize(data)
      @data = data
    end

    def zip_code
      data['zip_code']
    end

    def cbsa
      data['cbsa']
    end

    def msa
      data['msa']
    end

    def pop_2015
      data['pop_2015']
    end

    def pop_2014
      data['pop_2014']
    end
  end
end
