# PopulationGrowth

This is an api client for the Population Growth API

## Installation

I published this gem on RubyGems so the below instructions should work for pulling it down.

Add this line to your application's Gemfile:

```ruby
gem 'population_growth'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install population_growth

If neither of those options above work, you can just clone this repo and do a `bundle console` or build it locally in order to play with it.

## Usage

Every request will go through the `PopulationGrowth::Client` object. To start, instantiate it

`client = PopulationGrowth::Client.new` If we had access token requirements then we would pass them in here.

To get population data for a particular location

`location = client.locations.find(zip: 'your_zip_code_here')`

`location` will now have the following methods:

```ruby
location.zip_code
location.cbsa
location.msa
location.pop_2015
location.pop_2014
location.data
```

`location.data` provides a hash with all the attributes at once

## Discussion

- `client.locations` returns an instance of `PopulationGrowth::LocationsConnection` which contains all the avaialable actions we can take for the locations resource (get, create, update, etc.)
- `client.locations.find(zip: 'some_zip')` returns an instance of `PopulationGrowth::Location`, which is basically just a wrapper for the api data that comes in.

- The attempt here was to have each client method be an instance of a resource connection. It made more sense to me to keep all knowledge about how to interact with a particular resource into the resource connection object.
- I've worked on a couple API clients before where the client acts as the central hub and each method represents an single endpoint. If you have dozens of resources, each with multiple endpoints, then it becomes too difficult to maintain the client object.


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the PopulationGrowth project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/population_growth/blob/master/CODE_OF_CONDUCT.md).
