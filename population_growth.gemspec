
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "population_growth/version"

Gem::Specification.new do |spec|
  spec.name          = "population_growth"
  spec.version       = PopulationGrowth::VERSION
  spec.authors       = ["Peter Phan"]
  spec.email         = ["peter.m.phan@gmail.com"]

  spec.summary       = 'A client gem for working with the Population Growth API'
  spec.description   = 'A client gem for working with the Population Growth API'
  spec.homepage      = 'https://gitlab.com/pphan98/population_growth_client'
  spec.license       = "MIT"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency 'faraday', '~> 0.15.2'
  spec.add_runtime_dependency 'multi_json', '~> 1.13.1'
  spec.add_runtime_dependency 'oj', '~> 3.6.5'

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency 'webmock', '~> 3.4.2'
end
