require 'spec_helper'

module PopulationGrowth
  RSpec.describe Location do
    let(:location) { Location.new(data) }

    let(:data) do
      {
        'zip_code' => '90210',
        'cbsa' => '31080',
        'msa' => 'Los Angeles-Long Beach-Anaheim, CA',
        'pop_2015' => 13_340_068,
        'pop_2014' => 13_254_397,
      }
    end

    describe 'attributes' do
      it 'should have read methods for all properties of the response' do
        expect(location.zip_code).to eq('90210')
        expect(location.cbsa).to eq('31080')
        expect(location.msa).to eq('Los Angeles-Long Beach-Anaheim, CA')
        expect(location.pop_2015).to eq(13_340_068)
        expect(location.pop_2014).to eq(13_254_397)
        expect(location.data).to eq(data)
      end
    end
  end
end
