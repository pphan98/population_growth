require 'spec_helper'

module PopulationGrowth
  RSpec.describe Client do
    let(:client) { Client.new }

    describe '#locations' do
      it 'should return instance of LocationConnection' do
        expect(client.locations).to be_an_instance_of(LocationsConnection)
      end
    end
  end
end
