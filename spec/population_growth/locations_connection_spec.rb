require 'spec_helper'

module PopulationGrowth
  RSpec.describe LocationsConnection do
    let(:connection) { Faraday.new(url: 'https://population-growth-api.herokuapp.com') }
    let(:locations_connection) { LocationsConnection.new(connection: connection) }
    let(:zip_code) { '90210' }

    context 'successful request' do
      before do
        stub_request(:get, "https://population-growth-api.herokuapp.com/api/v1/locations/#{zip_code}")
          .to_return(
            status: 200, body: "{\"location\":{\"zip_code\":\"90210\",\"cbsa\":\"31080\",\"msa\":\"Los Angeles-Long Beach-Anaheim, CA\",\"pop_2015\":13340068,\"pop_2014\":13254397}}"
          )
      end

      describe '#find' do
        it 'should make a remote call to grab data and return a Location' do
          expect(locations_connection.find(zip: zip_code)).to be_instance_of(Location)
        end

        it 'should pass in the correct data to Location' do
          expect(Location).to receive(:new).with(
            'zip_code' => '90210',
            'cbsa' => '31080',
            'msa' => 'Los Angeles-Long Beach-Anaheim, CA',
            'pop_2015' => 13_340_068,
            'pop_2014' => 13_254_397,
          )
          locations_connection.find(zip: zip_code)
        end
      end
    end

    context 'failed request' do
      before do
        stub_request(:get, "https://population-growth-api.herokuapp.com/api/v1/locations/#{zip_code}")
          .to_return(status: 422)
      end

      describe '#find' do
        it 'should raise the proper exception with error message' do
          expect { locations_connection.find(zip: zip_code) }.to raise_error(ApiError, '422: ')
        end
      end
    end
  end
end
